/**
 * @format
 */
 import React from "react";
import {AppRegistry} from 'react-native';
import App from './Main/App'
import {name as appName} from './app.json';
import store from "./Main/redux/reducers/index";
import {Provider} from "react-redux";
const AppWithStore = () => {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    );
};

AppRegistry.registerComponent(appName, () => AppWithStore);

