import React, { useEffect,useState } from "react";
import { StyleSheet, View, Text, Image, Platform, ImageBackground, TouchableOpacity, Alert } from "react-native";
import styles from './style';
import { useDispatch, useSelector } from "react-redux";
import CONFIGURATION from "../../Component/Config/Config";
import GeneralStatusBar from "../../Component/GeneralStatusBar";
import Button from "../../Component/Button";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { logoutAuth } from "../../redux/action/auth";
import { useFocusEffect } from '@react-navigation/native';
import Api from './../../Component/API/index'
import RNFetchBlob from 'react-native-fetch-blob'
import ImgToBase64 from 'react-native-image-base64';
import NetInfo from "@react-native-community/netinfo";
import Toast from "../../Component/Tosat";

const App = (props) => {
  const dispatch = useDispatch();
  const { loginRes } = useSelector(state => state.login);
  const [toast, settoast] = useState(false)
  const [toasttext, settoasttext] = useState("")
  const [loadervisible, setloadervisible] = useState(false);
  const [types, settypes] = useState("");



  useEffect(() => {
    props.navigation.addListener('focus', async () => {      
      postdata()
    })
  }, [])
  

  const postdata = async () => {
    const createPro = await AsyncStorage.getItem(CONFIGURATION.createPost)
    console.log(createPro);
      var netState  = await NetInfo.fetch()
      console.log(netState.isConnected);
      if (netState.isConnected) {         
        if (createPro) {
          setloadervisible(true)
          var data = JSON.parse(createPro)[0]
          RNFetchBlob.fetch('POST', `${Api.BASE_URL}/posts/`, {
            "Authorization": `Bearer ${loginRes.access}`,
            'Content-Type': 'multipart/form-data',
          }, [
            { name: 'media_list', filename: data.image.fileName, type: data.image.type, data: data.image.data },
            { name: 'title', data: data.title },
            { name: 'category', data: data.category },
            { name: 'website', data: data.website },
            { name: 'description', data: data.des },
          ]).then(async(resp) => {
            setloadervisible(false)
            console.log("asdasdasd", resp);
            await AsyncStorage.removeItem(CONFIGURATION.createPost)
            await AsyncStorage.removeItem(CONFIGURATION.userPost)
          }).catch(async(error)=>{
            setloadervisible(false)
            await AsyncStorage.removeItem(CONFIGURATION.createPost)
            await AsyncStorage.removeItem(CONFIGURATION.userPost)
          })

        }
      }
  }

  return (
    <View style={styles.container} >
      <GeneralStatusBar backgroundColor={CONFIGURATION.primaryColor} />
      <View style={styles.btnView} >
        <Button
          backgroundColor={CONFIGURATION.primaryLight}
          height={50}
          borderRadius={15}
          width={"50%"}
          fontSize={18}
          fontColor={CONFIGURATION.white}
          title={"Create Post"}
          btnClick={() => {
            props.navigation.navigate("CreatePost")
          }}
          fontBold
        />
      </View>
      <View style={styles.btnView} >
        <Button
          backgroundColor={CONFIGURATION.primaryLight}
          height={50}
          borderRadius={15}
          width={"50%"}
          fontSize={18}
          fontColor={CONFIGURATION.white}
          title={"Post List"}
          btnClick={() => {
            props.navigation.navigate("PostList")
          }}
          fontBold
        />
      </View>
      <View style={styles.btnView} >
        <Button
          backgroundColor={CONFIGURATION.primaryLight}
          height={50}
          borderRadius={15}
          width={"50%"}
          fontSize={18}
          fontColor={CONFIGURATION.white}
          title={"Logout"}
          btnClick={async () => {
            // props.navigation.navigate("OrderList")
            await AsyncStorage.removeItem(CONFIGURATION.userData)
            await AsyncStorage.removeItem(CONFIGURATION.userPost)
            dispatch(logoutAuth({}))
          }}
          fontBold
        />
      </View>
      <Toast
        visible={toast}
        onPress={() => settoast(false)}
        label={toasttext}
        type={types}
        onClose={() => { console.log("close"); settoast(false) }}
        loadervisible={loadervisible}
      />
    </View>
  );
};

export default App;  