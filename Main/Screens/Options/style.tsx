import { StyleSheet, Dimensions } from "react-native";
const { height,width } = Dimensions.get("screen")
import CONFIGURATION from "../../Component/Config/Config";

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:CONFIGURATION.primaryColor,
      alignItems:"center",
      justifyContent:"center"
    },
    btnView:{
      marginVertical:10,
      width:width,
      alignItems:"center",
      justifyContent:"center"
    }
});

