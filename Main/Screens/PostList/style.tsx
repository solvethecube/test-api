import { StyleSheet, Dimensions } from "react-native";
const { height,width } = Dimensions.get("screen")
import CONFIGURATION from "../../Component/Config/Config";

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:CONFIGURATION.primaryColor
    },
    flatListView:{
      borderColor:CONFIGURATION.white,
      borderWidth:0.5,
      paddingHorizontal:20,
      paddingVertical:20,
      marginHorizontal:20,
      marginTop:30,
      borderRadius:15,
      marginBottom:1
    },
    titletext:{
      fontSize:20,
      color:CONFIGURATION.white,
      fontWeight:"bold",
      textAlign:"center",
      marginVertical:20
    },
    texth:{
      fontSize:15,
      color:CONFIGURATION.white,
      fontWeight:"200"
    },
    textt:{
      fontWeight:"bold"
    },
    imageBg:{
      height:70,
      width:70,
      backgroundColor:CONFIGURATION.white,
      position:"absolute",
      borderRadius:15,
      top:-20,
      right:20,
      overflow:"hidden",
      alignItems:"center",
      justifyContent:"center"
    },
    Image:{
      height:70,
      width:70,
      borderRadius:15,
      borderWidth:0.5,
      borderColor:CONFIGURATION.white
    }
});

