import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, Image, Platform, ImageBackground, TouchableOpacity, FlatList } from "react-native";
import styles from './style';
import { useDispatch, useSelector } from "react-redux";
import CONFIGURATION from "../../Component/Config/Config";
import GeneralStatusBar from "../../Component/GeneralStatusBar";
import Api from '../../Component/API/index'
import Toast from "../../Component/Tosat";
import AsyncStorage from "@react-native-async-storage/async-storage";
import NetInfo from "@react-native-community/netinfo";

const App = (props) => {
  const dispatch = useDispatch();
  const { loginRes } = useSelector(state => state.login);
  const [toast, settoast] = useState(false)
  const [toasttext, settoasttext] = useState("")
  const [loadervisible, setloadervisible] = useState(false);
  const [types, settypes] = useState("");
  const [datas, setdatas] = useState([])


  useEffect(() => {
    CheckList()
  }, [])


  const CheckList = async () => {
    const data = await AsyncStorage.getItem(CONFIGURATION.userPost)
    console.log("==>", data);
    if (data) {
      setdatas(JSON.parse(data))
      GetList()
    } else {
      setloadervisible(true)
      GetList()
    }
  }

  const GetList = async () => {
    NetInfo.addEventListener(async netState => {
      console.log(netState.isConnected);
      if (netState.isConnected) {
        try {
          
          const { data } = await Api.GetList()
          console.log(data);
          if (data.status == 200) {
            console.log(JSON.stringify(data.data.results));
            setdatas(data.data.results)
            await AsyncStorage.setItem(CONFIGURATION.userPost, JSON.stringify(data.data.results))
            setloadervisible(false)
          } else {
            if(data.statusText){
              setloadervisible(false)
              settoasttext(data.statusText)
              settoast(true)
              settypes("")
            }
          }
        } catch (error) {
          setloadervisible(false)
          settoasttext("Something went wrong.")
          settoast(true)
          settypes("")
        }
      } else {
        const datas = await AsyncStorage.getItem(CONFIGURATION.userPost)
        setdatas(JSON.parse(datas))
      }
    })
  }
  return (
    <>
      <View style={styles.container} >
        <GeneralStatusBar backgroundColor={CONFIGURATION.primaryColor} />
        <Text style={styles.titletext} >Post List</Text>
        <FlatList
          data={datas}
          renderItem={({ item }) => {
            console.log();
            return (
              <View style={styles.flatListView} >
                <Text style={styles.texth} >Title: <Text style={styles.textt} >{item.title}</Text></Text>
                <Text style={styles.texth} >Category: <Text style={styles.textt} >{item.category}</Text></Text>
                <Text style={styles.texth} >Description: <Text style={styles.textt} >{item.description}</Text></Text>
                <Text style={styles.texth} >Website: <Text style={styles.textt} >{item.website}</Text></Text>
                <View style={styles.imageBg} >
                  {
                    item.materials[0].post_type == null ?
                      <Image style={styles.Image} source={{ uri: item.materials[0].media_file }} />
                      :
                      <Image style={styles.Image} source={{ uri: CONFIGURATION.BaseUri + item.materials[0].media_file }} />
                  }
                  {/* <Image style={styles.Image} source={{ uri: CONFIGURATION.BaseUri + item.materials[0].media_file }} /> */}
                </View>
              </View>
            )
          }}
          keyExtractor={item => item.id}
        />
      </View>
      <Toast
        visible={toast}
        onPress={() => settoast(false)}
        label={toasttext}
        type={types}
        onClose={() => { console.log("close"); settoast(false) }}
        loadervisible={loadervisible}
      />
    </>
  );
};

export default App;  