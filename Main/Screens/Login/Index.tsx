import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, Image, Platform, ImageBackground, TouchableOpacity, TextInput, Dimensions, Alert } from "react-native";
import styles from './style';
import { useDispatch, useSelector } from "react-redux";
import CONFIGURATION from "../../Component/Config/Config";
import GeneralStatusBar from "../../Component/GeneralStatusBar";
import Icon from 'react-native-vector-icons/FontAwesome'
import Button from "../../Component/Button";
import { loginAuth } from "../../redux/action/auth";
import Toast from "../../Component/Tosat";
const { height, width } = Dimensions.get("screen")
import Api from './../../Component/API/index'
import AsyncStorage from "@react-native-async-storage/async-storage";
const App = (props) => {
  const dispatch = useDispatch();
  const [userName, setuserName] = useState("developer25")
  const [password, setpassword] = useState("Now@12345")
  const [toast, settoast] = useState(false)
  const [toasttext, settoasttext] = useState("")
  const [loadervisible, setloadervisible] = useState(false);
  const [types, settypes] = useState("");



  const login = async () => {
    setloadervisible(true)
    try {
      const body = {
        username: userName,
        password: password
      }
      const { data } = await Api.LoginApi(body)
      console.log(data);
      
      if (data.status == 200) {
        setloadervisible(false)
        settoasttext("Successful Login.")
        settoast(true)
        settypes("sucess")
        setTimeout(async() => {         
          await AsyncStorage.setItem(CONFIGURATION.userData,JSON.stringify(data.data))
          dispatch(loginAuth(data.data))
        }, 3000);
      } else {
        setloadervisible(false)
        settoasttext(data.statusText)
        settoast(true)
        settypes("")
      }
    } catch (error) {
      setloadervisible(false)
      settoasttext("Something went wrong.")
      settoast(true)
      settypes("")
    }
  }

  return (
    <>
      <View style={styles.container} >
        <GeneralStatusBar backgroundColor={CONFIGURATION.primaryColor} />
        <Text style={styles.loginText} >LOGIN</Text>
        <View style={[styles.inputView, { borderColor: userName ? CONFIGURATION.white : CONFIGURATION.white + 50, marginTop: 50 }]} >
          <Icon name="user" size={20} color={userName ? CONFIGURATION.white : CONFIGURATION.white + 50} />
          <TextInput
            style={styles.input}
            placeholder="UserName"
            placeholderTextColor={CONFIGURATION.white + 50}
            onChangeText={(text) => {
              setuserName(text)
            }}
            value={userName}
          />
        </View>
        <View style={[styles.inputView, { borderColor: password ? CONFIGURATION.white : CONFIGURATION.white + 50, marginTop: 20 }]} >
          <Icon name="lock" size={20} color={password ? CONFIGURATION.white : CONFIGURATION.white + 50} />
          <TextInput
            style={styles.input}
            placeholder="UserName"
            placeholderTextColor={CONFIGURATION.white + 50}
            onChangeText={(text) => {
              setpassword(text)
            }}
            secureTextEntry
            value={password}
          />
        </View>
        <View style={styles.btnView} >
          <Button
            backgroundColor={userName && password ? CONFIGURATION.primaryLight : CONFIGURATION.primaryLight + 50}
            height={50}
            borderRadius={15}
            width={"50%"}
            fontSize={18}
            fontColor={userName && password ? CONFIGURATION.white : CONFIGURATION.white + 50}
            title={"Login"}
            btnClick={() => {
              if (userName && password) {
                login()
              } else {
                settoasttext("Please Enter UserName & Password.")
                settoast(true)
                settypes("")
              }
            }}
          />
        </View>
      </View>
      <Toast
        visible={toast}
        onPress={() => settoast(false)}
        label={toasttext}
        type={types}
        onClose={() => { console.log("close"); settoast(false) }}
        loadervisible={loadervisible}
      />
    </>
  );
};

export default App;  