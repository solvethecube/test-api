import { StyleSheet, Dimensions } from "react-native";
const { height,width } = Dimensions.get("screen")
import CONFIGURATION from "../../Component/Config/Config";

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:CONFIGURATION.primaryColor,
      alignItems:"center",
      justifyContent:"center"
    },
    loginText:{
      fontWeight:"bold",
      fontSize:35,
      color:CONFIGURATION.white
    },
    inputView:{
      borderWidth:1,
      width:width-60,
      flexDirection:"row",
      alignItems:"center",
      borderRadius:15,
      paddingHorizontal:20,
      justifyContent:"space-between"
    },
    input:{
      height:45,
      width:"90%",
      color:CONFIGURATION.white
    },
    btnView:{
      width:width,
      marginTop:30,
      alignItems:"center",
      justifyContent:"center",
      marginBottom:50
    }
});

