import { StyleSheet, Dimensions } from "react-native";
const { height,width } = Dimensions.get("screen")
import CONFIGURATION from "../../Component/Config/Config";

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:CONFIGURATION.primaryColor
    },
    titletext:{
      fontSize:20,
      color:CONFIGURATION.white,
      fontWeight:"bold",
      textAlign:"center",
      marginVertical:20
    },
    inputTitle:{
      fontSize:12,
      color:CONFIGURATION.white,
      paddingHorizontal:10,
      backgroundColor:CONFIGURATION.primaryColor,
      position:"absolute",
      top:-8,
      left:25
    },
    inputView:{
      borderColor:CONFIGURATION.white,
      borderWidth:0.5,
      marginHorizontal:20,
      borderRadius:15,
      height:55,
      justifyContent:"center",
      paddingHorizontal:20,
      marginTop:20
    },
    input:{
      height:50,
      marginTop:0,
      color:CONFIGURATION.white
    },
    btnView:{
      alignItems:"center",
      justifyContent:"center",
      backgroundColor:CONFIGURATION.primaryColor,
      paddingBottom:20
    }
});

