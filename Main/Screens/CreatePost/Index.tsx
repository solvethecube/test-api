import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, Image, Platform, ImageBackground, Dimensions, Modal, TouchableOpacity, TextInput, ScrollView, Alert, } from "react-native";
import styles from './style';
import { useDispatch, useSelector } from "react-redux";
import CONFIGURATION from "../../Component/Config/Config";
import GeneralStatusBar from "../../Component/GeneralStatusBar";
import Button from "../../Component/Button";
import RNFetchBlob from 'react-native-fetch-blob'
import ImagePicker from 'react-native-image-picker';
const { height, width } = Dimensions.get("screen")
import Api from '../../Component/API/index'
import Toast from "../../Component/Tosat";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ModalComponent from './../../Component/cameraModal'
const App = (props) => {
  const dispatch = useDispatch();
  const { loginRes } = useSelector(state => state.login);
  const [title, settitle] = useState("")
  const [category, setcategory] = useState("")
  const [website, setwebsite] = useState("")
  const [image, setimage] = useState({})
  const [des, setdes] = useState("")
  const [selectPic, setselectPic] = useState(false)
  const [toast, settoast] = useState(false)
  const [toasttext, settoasttext] = useState("")
  const [loadervisible, setloadervisible] = useState(false);
  const [types, settypes] = useState("");
  const [isConnected, setisConnected] = useState(true)
  const images = () => {
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User Cancelled Image Picker');
      }
      else if (response.error) {
        console.log('Image Picker Error = ', response.error);
      }
      else if (response.customButtons) {
        console.log('User tapped custom button: ', response.customButtons);
        alert(response.customButtons);
      }
      else {
        console.log('Response = ', response);
        setimage(response)
      }
    })
  };

  const camera = () => {
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel) {
        console.log('User Cancelled Image Picker');
      }
      else if (response.error) {
        console.log('Image Picker Error = ', response.error);
      }
      else if (response.customButtons) {
        console.log('User tapped custom button: ', response.customButtons);
        alert(response.customButtons);
      }
      else {
        console.log('Response = ', response);
        setimage(response)
      }
    })
  }

  useEffect(() => {
    NetInfo.addEventListener(netState => {
      setisConnected(netState.isConnected)
    });
  }, [])


  const CreatePost = async () => {
    var state = await NetInfo.fetch()

    // return
    if (state.isConnected) {

      try {
        setloadervisible(true)
        RNFetchBlob.fetch('POST', `${Api.BASE_URL}/posts/`, {
          "Authorization": `Bearer ${loginRes.access}`,
          'Content-Type': 'multipart/form-data',
        }, [
          { name: 'media_list', filename: image.fileName, type: image.type, data: image.data },
          { name: 'title', data: title },
          { name: 'category', data: category },
          { name: 'website', data: website },
          { name: 'description', data: des },
        ]).then((resp) => {
          console.log(JSON.parse(resp.data));
          if (JSON.parse(resp.data).errors) {
            setloadervisible(false)
            settoasttext("Something went wrong.")
            settoast(true)
            settypes("")
          } else {
            setloadervisible(false)
            settoasttext(JSON.parse(resp.data).message)
            settoast(true)
            settypes("sucess")
          }
          // ...
        }).catch((err) => {
          setloadervisible(false)
          settoasttext("Something went wrong.")
          settoast(true)
          settypes("")
          // ...
        })
        // console.log(data);
      } catch (error) {
        console.log(error);
      }
    } else {

      const postData = await AsyncStorage.getItem(CONFIGURATION.userPost)
      const postdata = await AsyncStorage.getItem(CONFIGURATION.createPost)
      var posts = [{
        "title": title,
        "image": image,
        "category": category,
        "website": website,
        "des": des
      }]
      await AsyncStorage.setItem(CONFIGURATION.createPost, JSON.stringify(posts))

      var creactNewPostData2 = [{
        "id": "3e73470d-b6f3-4e37-9735-73bba7cb7ba3",
        "post_type": "general",
        "views_count": 0,
        "materials": [{
          "id": 1021,
          "media_file": image.uri,
          "created_at": "2022-02-18T10:22:43.476139Z",
          "updated_at": "2022-02-18T10:22:43.476162Z",
          "post_type": null
        }],
        "creator_details": {
          "id": 1097,
          "username": "developer25",
          "name": "Akash nitrx",
          "avatar": "/media/images/profile_images/user.png",
          "verified_account": false
        },
        "liked": false,
        "follow": false,
        "rated": false,
        "rated_value": 0,
        "is_post_saved": false,
        "comments_rating_average": 0,
        "rating_count": null,
        "comment_count": 0,
        "created_at": 1645179763.425415,
        "updated_at": 1645179763.470208,
        "title": title,
        "keywords": null,
        "description": des,
        "website": website,
        "post_qr": "/media/images/post_qr_codes/1028.png",
        "weekly_impression": 0,
        "count_comment_nitrx": {
          "nitrx": 0,
          "comments": 0
        },
        "category": category,
        "creator": 1097
      }]
      // alert(state.isConnected)
      var datas = []
      if (postData) {
        datas = [...creactNewPostData2, ...JSON.parse(postData)]
      } else {
        datas = [...creactNewPostData2]
      }

      console.log(datas);

      await AsyncStorage.setItem(CONFIGURATION.userPost, JSON.stringify(postData ? datas : creactNewPostData2))

      setloadervisible(false)
      settoasttext("Successful create post.")
      settoast(true)
      settypes("sucess")
    }
  }

  return (
    <>
      <View style={styles.container} >
        <GeneralStatusBar backgroundColor={CONFIGURATION.primaryColor} />
        <Text style={styles.titletext} >Create Post</Text>
        <ScrollView>
          <View style={styles.inputView} >
            <Text style={styles.inputTitle} >Title</Text>
            <TextInput
              style={styles.input}
              placeholderTextColor={CONFIGURATION.white + 50}
              placeholder="Title"
              onChangeText={(text) => {
                settitle(text)
              }}
              value={title}
            />
          </View>
          <View style={styles.inputView} >
            <Text style={styles.inputTitle} >Category</Text>
            <TextInput
              style={styles.input}
              placeholderTextColor={CONFIGURATION.white + 50}
              placeholder="Category"
              onChangeText={(text) => {
                setcategory(text)
              }}
              value={category}
            />
          </View>
          <View style={styles.inputView} >
            <Text style={styles.inputTitle} >Website</Text>
            <TextInput
              style={styles.input}
              placeholderTextColor={CONFIGURATION.white + 50}
              placeholder="Website"
              onChangeText={(text) => {
                setwebsite(text)
              }}
              value={website}
            />
          </View>
          <TouchableOpacity onPress={() => {
            setselectPic(true)
          }} style={styles.inputView} >
            <Text style={styles.inputTitle} >Select Image</Text>
            <TextInput
              style={styles.input}
              placeholderTextColor={CONFIGURATION.white + 50}
              placeholder="Select Image"
              editable={false}
              onChangeText={(text) => {
                setimage(text)
              }}
              value={image ? image.fileName : ""}
            />
          </TouchableOpacity>
          <View style={styles.inputView} >
            <Text style={styles.inputTitle} >Description</Text>
            <TextInput
              style={styles.input}
              placeholderTextColor={CONFIGURATION.white + 50}
              placeholder="Description"
              onChangeText={(text) => {
                setdes(text)
              }}
              value={des}
            />
          </View>
        </ScrollView>
      </View>
      <View style={styles.btnView} >
        <Button
          backgroundColor={title && category && website && image && des ? CONFIGURATION.primaryLight : CONFIGURATION.primaryLight + 50}
          height={50}
          borderRadius={15}
          width={"50%"}
          fontSize={18}
          fontColor={title && category && website && image && des ? CONFIGURATION.white : CONFIGURATION.white + 50}
          title={"Submit"}
          btnClick={() => {
            try {
              CreatePost()
            } catch (e) {
              alert(e)
            }
          }}
          fontBold
        />
      </View>
      <ModalComponent open={selectPic} onClick={(res: string) => {
        if (res == "camera") {
          camera();
          setselectPic(false)
        } else if (res == "gallery") {
          images();
          setselectPic(false)
        } else {
          setselectPic(false)
        }
      }} />
      <Toast
        visible={toast}
        onPress={() => settoast(false)}
        label={toasttext}
        type={types}
        onClose={() => { console.log("close"); settoast(false) }}
        loadervisible={loadervisible}
      />
    </>
  );
};

export default App;  