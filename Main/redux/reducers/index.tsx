import { createStore, combineReducers } from 'redux';
import { login } from './login';

const rootReducer = combineReducers({
    login: login,
});

const store = createStore(rootReducer);

export default store