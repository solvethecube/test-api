import { USER_DATA_LOGOUT ,USER_DATA_LOGIN} from "../types";

const initialState = {
    isSignIn: false,
    loginRes: {},
}

export function login(state = initialState, action: { type: any; payload: any; }) {
    switch (action.type) {
        case USER_DATA_LOGOUT:
            return {
                ...state,
                isSignIn: false,
                loginRes: action.payload
            }
            case USER_DATA_LOGIN:
            return {
                ...state,
                isSignIn: true,
                loginRes: action.payload
            }
        default:
            return state
    }
}