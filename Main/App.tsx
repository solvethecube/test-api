import React, {useEffect} from 'react';
import {View, Image, NetInfo, Alert} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {useDispatch, useSelector} from 'react-redux';

// Screens
import Login from './Screens/Login/Index';
import Options from './Screens/Options/Index';
import PostList from './Screens/PostList/Index';
import CreatePost from './Screens/CreatePost/Index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CONFIGURATION from './Component/Config/Config';
import { loginAuth } from './redux/action/auth';
import { useFocusEffect } from '@react-navigation/native';

const Stack = createStackNavigator();

const AppWithNavContainer = () => {
  const dispatch = useDispatch();
  const { isSignIn } = useSelector(state => state.login);
  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    const data = await AsyncStorage.getItem(CONFIGURATION.userData)
    console.log("==>",data);
    if(data){
      dispatch(loginAuth(JSON.parse(data)))
    }
  }

  return (
    <NavigationContainer>
      { !isSignIn ? (
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
          />
        </Stack.Navigator>
      ) : (
        <Stack.Navigator headerMode="none" initialRouteName="Options">
          <Stack.Screen
            name="Options"
            component={Options}
            options={{
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
          />
          <Stack.Screen
            name="PostList"
            component={PostList}
            options={{
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
          />
          <Stack.Screen
            name="CreatePost"
            component={CreatePost}
            options={{
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
          />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
};

export default AppWithNavContainer;
