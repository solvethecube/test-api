import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CONFIGURATION from './../Config/Config';

const API_URL = `${CONFIGURATION.BaseUri}/api/v1`;

const AuthServices = {
    LoginApi: async (body) => {
        try {
            const config = {
                headers: {
                    'Content-Type': `application/json`,
                },
            };

            console.log("sgfsdgf", `${API_URL}/auth/login/`);
            const data = await axios.post(`${API_URL}/auth/login/`, body, config);
            return { error: false, data: data };
        }
        catch (e) {
            return { error: true, data: e };
        }
    },
    CreatePost: async (body) => {
        try {
            const userdata = await AsyncStorage.getItem(CONFIGURATION.userData)
            var token = JSON.parse(userdata).access
            const config = {
                headers: {
                    'Content-Type': `multipart/form-data`,
                    'Authorization': `Bearer ${token}`
                },
            };
            console.log(config);
            console.log("sgfsdgf", `${API_URL}/posts/`);
            const data = await axios.post(`${API_URL}/posts/`, body, config);
            return { error: false, data: data };
        }
        catch (e) {
            return { error: true, data: e };
        }
    },
    GetList: async () => {
        try {
            const userdata = await AsyncStorage.getItem(CONFIGURATION.userData)
            var token = JSON.parse(userdata).access
            const config = {
                headers: {
                    'Content-Type': `multipart/form-data`,
                    'Authorization': `Bearer ${token}`
                },
            };
            console.log(config);
            console.log("sgfsdgf", `${API_URL}/posts/`);
            const data = await axios.get(`${API_URL}/posts/`, config);
            return { error: false, data: data };
        }
        catch (e) {
            return { error: true, data: e };
        }
    },
}
export default AuthServices;