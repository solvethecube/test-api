import React from 'react';
import { View, TouchableOpacity, Text, Modal, Dimensions } from 'react-native';
import CONFIGURATION from './Config/Config';
const { height, width } = Dimensions.get("screen");

interface modeldata {
    open: boolean
    onClick: Function
}

const Component: React.FC<modeldata> = ({
    open,
    onClick
}) => {
    return (
        <>
            <Modal
                animationType="fade"
                transparent={true}
                visible={open}
                onRequestClose={() => {

                }}
            >
                <TouchableOpacity activeOpacity={1} onPress={() => { onClick("false") }} style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: CONFIGURATION.white + 50,
                }}>
                    <TouchableOpacity activeOpacity={1} style={{
                        margin: 20,
                        backgroundColor: CONFIGURATION.primaryColor,
                        borderRadius: 10,
                        paddingVertical: 20,
                        paddingHorizontal: 20,
                        alignItems: "center",
                    }}>
                        <TouchableOpacity onPress={async () => {
                            onClick("camera")
                        }} style={{ height: 50, width: width - 100, alignItems: "center", justifyContent: "center", borderColor: CONFIGURATION.white + 20, borderBottomWidth: 0.5 }}>
                            <Text style={{ fontSize: 20, color: CONFIGURATION.white, }}>Camera</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { onClick("gallery") }} style={{ height: 50, width: width - 100, alignItems: "center", justifyContent: "center", borderColor: CONFIGURATION.white + 20, borderBottomWidth: 0.5 }}>
                            <Text style={{ fontSize: 20, color: CONFIGURATION.white, }}>Gallery</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal>
        </>
    );
}

export default Component;