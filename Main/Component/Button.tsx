import React from 'react';
import { TouchableOpacity, TouchableOpacityProps, View, Text } from 'react-native';
import CONFIGURATION from './Config/Config';

export interface TouchableParams extends TouchableOpacityProps {
    width?: any;
    height?: any;
    backgroundColor:string;
    borderRadius?:number;
    title:string;
    fontSize:number;
    fontColor?:string;
    btnClick:Function;
    fontBold?:boolean;
}

const Button: React.FC<TouchableParams> = (props) => {
  return(
      <TouchableOpacity onPress={props.btnClick} style={{height:props.height ? props.height : 50,width:props.width ? props.width : 150,backgroundColor:props.backgroundColor,borderRadius:props.borderRadius,alignItems:"center",justifyContent:"center"}} >
          <Text style={{fontSize:props.fontSize,color:props.fontColor ? props.fontColor : CONFIGURATION.white,fontWeight:props.fontBold ? "bold" : "normal"}} >{props.title}</Text>
      </TouchableOpacity>
  );
}

export default Button;