const CONFIGURATION = {

    //Colors
    primaryColor: '#463d90',
    primaryLight: '#08c6ac',
    white: "#ffffff",
    black: "#000000",

    //Asyc Key
    userData:"USER_DATA",
    userPost:"USER_POST",
    createPost:"USER_CREATE_POST",

    //Base Uri
    BaseUri:"https://api.nitrx.com"
}

export default CONFIGURATION
